#pragma once

namespace asd{



	template<typename T>
	void quick_sort(T*& items, int left, int right) {

		int i = left, j = right, center = items[(left+right)/2];

		T substitution;

		do {

			while ((items[i] < center) && (i < right))i++;
			while ((items[j] > center) && (j > left))j--;

			if (i <= j) {

				substitution = items[i];
				items[i] = items[j];
				items[j] = substitution;
				i++; j--;

			}

		} while (i <= j);

		if (left < j) quick_sort(items, left, j);
		if (i < right) quick_sort(items, i, right);


	}

	template<typename T>
	void reverse_counting_sort(T*& massive, int n, int &exp, int & limit) {

		static int protect = 0;

		protect++;

		T* substitution = new T[(unsigned long long)n+1];

		substitution[n] = 0;

		int count[10] = { 0 };

		for (int i = 0; i < n; i++)
			count[massive[i] / exp % 10]++;

		for (int i = 1; i < 10; i++)count[i] += count[i - 1];

		for (int i = n - 1; i >= 0; i--) {

			substitution[count[massive[i] / exp % 10] - 1] = massive[i];
			count[massive[i] / exp % 10]--;

		}

		if (protect == limit) {

			protect = 0;
			for (int i = 0, j = n - 1; i < n; i++, j--)
				massive[i] = substitution[j];

		}

		else for (int i = 0; i < n; i++)
			massive[i] = substitution[i];

		delete[] substitution;

	}

	template<typename T>
	int get_max(T*& massive, int n) {

		int max = massive[0];

		for (int i = 1; i < n; i++)
			if (max < massive[i])
				max = massive[i];

		return max;
	}

	template<typename T>
	void reverse_radix_sort(T*& massive, int n) {

		int max = get_max(massive, n);
		
		int i = 0,sub_exp=1;

		for (; max / sub_exp > 0; i++, sub_exp*=10);

		for (int exp = 1; max / exp > 0; exp *= 10) {
			reverse_counting_sort(massive, n, exp,i);
		}

	}

}