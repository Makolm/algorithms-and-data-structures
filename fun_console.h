#pragma once

#include <windows.h>

enum console_color{

	black = 0,
	blue = 1,
	green = 2,
	cyan = 3,
	red = 4,
	magenta = 5,
	brown = 6,
	light_gray = 7,
	dark_gray = 8,
	light_blue = 9,
	light_green = 10,
	light_cyan = 11,
	light_red = 12,
	light_magenta = 13,
	yellow = 14,
	white = 15

};

void go_to(short x, short y);
void cls();
void set_color(short textcolor, short bkcolor);
void set_size(int X, int Y);
unsigned int get_rows();
unsigned int get_cols();
unsigned int get_current_y();
unsigned int get_current_x();
void show_caret(BOOL show = TRUE);