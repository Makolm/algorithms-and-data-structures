#include "window_read.h"
#include <conio.h>
#pragma warning(disable:4996)

namespace asd {

	window_read::window_read() {

		user_width = 50, user_height = 20;

		for_user.set_size(user_width, user_height);

		for_user.set_align(asd::window::center | asd::window::middle);

		wnd_success.set_size(user_width - for_user.get_border() * 2, for_user.get_client_height() / 2);
		wnd_success.set_border_color(blue);
		wnd_success.set_bg_color(green);

		info_x = for_user.get_x() + for_user.get_border();
		info_x_border = info_x + wnd_success.get_border();
		wnd_y = for_user.get_y() + for_user.get_border();
		info_y = wnd_y + for_user.get_client_height() / 2;

		
		wnd_success.move(info_x, info_y);

	}

	void window_read::read_pos(unsigned x, unsigned y, unsigned len, read& object) {

		object.set_lenght(len);
		object.set_position(x, y);

	}

	void window_read::write_str(methods_for_string value) {
		
		read object;

		read_pos(info_x, wnd_y, for_user.get_client_width(), object);

		if (value == percent_letter) {

			if (!object.text(read::only_letters)) return;

			show_info(object.get(), percent_letter);

		}

		else if (value == replace_letter) {

			if (!object.text(read::letters_space)) return;

			show_info(object.get(), replace_letter);

		}

		else if (value == count_words_space) {

			if (!object.text(read::letters_space)) return;

			show_info(object.get(), count_words_space);

		}

		else if (value == reverse) {

			if (!object.text(read::letters_space)) return;

			show_info(object.get(), reverse);

		}

		else if (value == replace_words) {

			if (!object.text(read::letters_space)) return;

			show_info(object.get(), replace_words);

		}

		else if (value == sorts) {

			if (!object.text(read::letters_digits)) return;

			const char strs[6][30] = {

				{"1: sorts"},
				{"2: numbers-sorts"},
				{"3: letters-sorts"},
				{"4: reverse-sorts"},
				{"5: reverse-numbers-sorts"},
				{"6: reverse-letters-sorts"}

			};

			for (unsigned i = 0; i < 6; i++) {

				::go_to(info_x, wnd_y + i + 1);

				std::cout << strs[i];

			}

			show_info(object.get(), sorts);

		}

		else if (value == find_letter_digit) {

			if (!object.text(read::letters_digits_space))return;

			::go_to(info_x, wnd_y + 1);

			std::cout << "find: 1		reverse-find: 2";

			show_info(object.get(), find_letter_digit);

		}


	}

	void window_read::write_vec(methods_for_vectors value, bool matrix) {

		unsigned limit = wnd_success.get_client_height()-2, number = 0, y_ = wnd_y+1;
		
		read key;

		read_pos(info_x + 14, wnd_y, 3, key);

		for_user.clear();

		std::cout << "count-tables: ";

		if (!key.text(read::only_digits) || !key.get()[0] || atoi(key.get()) > (int)limit) return;
		
		unsigned protect = (unsigned)atoi(key.get());

		for (unsigned i = 0; i < protect; i++,y_++) {

			read object;

			read_pos(info_x, y_, for_user.get_client_width(), object);

			if (!matrix && !object.text(read::digits_space)) return;

			else if (matrix && (!object.text(read::digits_space) ||
				protect != object.get().count_numbers())) return;

			if (!object.get()) return;

				asd::vector<int> sub;

				object.get().to_digit(sub);

				vec_vec.add(sub);

		}

		if (value == DMD_of_vector) {

			::go_to(info_x, info_y - 1);

			std::cout << "1: all | 2: odd | 3: even";

			show_info(DMD_of_vector,protect);

		}

	}

	void window_read::show() { for_user.show(); }

	void window_read::show_info(string str, methods_for_string method) {

		int len = str.lenght(), limit = wnd_success.get_client_width();

		wnd_success.show();

		switch (method) {

		case percent_letter: {

			char m = ' ';

			std::cout << "letter: ";
			while (!my_isalpha(m))m = _getch();
			std::cout << m;

			::go_to(info_x_border, info_y + wnd_success.get_border() + 1);

			std::cout << "percent: " << str % m;

			break;

		}

		case replace_letter: {

			char m = ' ', z = ' ';

			std::cout << "letter: ";

			while (!my_isalpha(m))
				m = _getch();

			std::cout << m << " " << "replace: ";

			while (!my_isalpha(z))
				z = _getch();

			std::cout << z  << " count: " << str.count_letters(m);

			::go_to(info_x_border, info_y + wnd_success.get_border() + 1);

			str.replace_letters(m, z);

			std::cout << str;

			break;

		}

		case count_words_space: {

			str.remove_extra_spaces();

			std::cout << str;

			::go_to(info_x_border, info_y + wnd_success.get_border() + 1);

			std::cout << "words: " << str.count_words();

			break;

		}

		case reverse: {

			str.reverse();

			std::cout << str;

			break;

		}

		case replace_words: {

			asd::read word;
			asd::read replace;

			int spaces = 0;

			word.set_lenght(wnd_success.get_client_width() / 2 - 6);

			while (!word.get()) {

				std::cout << "word: ";

				word.set_position(info_x_border + 6, info_y + wnd_success.get_border());


				word.text(read::only_letters);

				spaces = word.get().lenght();

			}

			replace.set_lenght(wnd_success.get_client_width() - spaces - 15);

			while (!replace.get()) {

				std::cout << " replace ";

				replace.set_position(info_x_border + 15 + spaces,
					info_y + wnd_success.get_border());

				replace.text(read::only_letters);

			}

			::go_to(info_x + wnd_success.get_border(), info_y + wnd_success.get_border() + 1);

			str.replace_words(word.get(), replace.get());

			for (int i = 0; i < len && i < limit; i++)
				std::cout << str[i];

			break;

		}

		case sorts: {

			char m = ' ';

			while (m < '1' || m>'6')
				m = _getch();

			if (m == '1')str.sort(1);

			else if (m == '2')str.sort_numbers(1);

			else if (m == '3')str.sort_letters(1);

			else if (m == '4')str.sort(-1);

			else if (m == '5')str.sort_numbers(-1);

			else if (m == '6')str.sort_letters(-1);

			std::cout << m;

			::go_to(info_x_border, info_y + wnd_success.get_border() + 1);

			for (int i = 0; i < len && i < limit; i++)
				std::cout << str[i];

			break;

		}

		case find_letter_digit: {

			char m = ' ', letter = ' ';

			std::cout << "method: ";

			while (m != '1' && m != '2')
				m = _getch();

			std::cout << m;

			::go_to(info_x_border + wnd_success.get_client_width() / 2
				, info_y + wnd_success.get_border());

			std::cout << "find: ";

			while (!my_isalpha(letter) && !my_isdigit(letter))
				letter = _getch();

			std::cout << letter;

			::go_to(info_x_border, info_y + wnd_success.get_border() + 1);

			if (m == '1')
				std::cout << str.find(letter, 1);

			else std::cout << str.find(letter, -1);

			break;

		}

		}

	}

	void window_read::show_info(methods_for_vectors method,unsigned count) {

		char* key = new char[3]; key[0] = key[1] = ' '; key[2] = 0;
		read info;

		int bor = wnd_success.get_border(), wid = wnd_success.get_client_width(), num = 0,y_=info_y+3,
			for_key = 0;

		wnd_success.show();

		if (method == DMD_of_vector) {

			// ����� � �� ������� �����

			std::cout << "method: "; while (key[0] < '1' || key[0] > '3') key[0] = _getch();

			std::cout << key;

			info.set_lenght(2); info.set_position(info_x + wid / 2 + 9, info_y + bor);

			go_to(info_x + wid / 2, info_y+ bor);

			std::cout << "number: "; 

			while (!info.get())
			if (!info.text(read::only_digits))return;

			// ��������

			go_to(info_x + wid / 2, info_y + bor+1);

			std::cout << "operation: ";

			while (key[1] != '*' && key[1] != '/' && key[1] != '%')
				key[1] = _getch();

			std::cout << key[1];

			num = atoi(info.get());
			for_key = atoi((char *)key);

			if (key[0] == '1')
				for (unsigned i = 0; i < count; i++) vec_vec[i].def_multiples(num, 0, key[1]);

			else if (key[0] == '2')
				for (unsigned i = 0; i < count; i++) vec_vec[i].def_multiples(num, 1, key[1]);

			else if (key[0] == '3')
				for (unsigned i = 0; i < count; i++)vec_vec[i].def_multiples(num, 2, key[1]);

			asd::string str;

			for (unsigned i = 0; i < count; i++,y_++) {

				int len = vec_vec[i].get_count();

				for (unsigned j = 0; j < len; j++)
					str.to_string(vec_vec[i][j]) += " ";

				len = str.lenght();

				::go_to(info_x+1, y_);

				for (unsigned j = 0; j < len && j < wid; j++)
					std::cout << str[j];

				str.empty();

			}

		}
		
		delete[] key;
		
	}

}