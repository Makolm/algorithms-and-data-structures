#pragma once
#include "asd_string.h"
#include "asd_vector.h"
#include "fun_console.h"
#include "read.h"

namespace asd {

	class window {

	protected:

		unsigned int color, bg_color, border_color;
		unsigned int x, y, w, h, border;
		unsigned char buf[6];

	public:

		enum {

			left = 1,
			center=2,
			right=4,
			top=8,
			middle=16,
			bottom=32

		};

		window();

		void set_color(unsigned int);
		void set_bg_color(unsigned int);
		void set_border_color(unsigned int);

		unsigned int get_client_width();
		unsigned int get_client_height();
		unsigned int get_border();

		unsigned int get_x();
		unsigned int get_y();

		void clear();
		void move(unsigned int, unsigned int);
		void set_size(unsigned int, unsigned int);
		void set_border(unsigned int);
	
		void set_align(char);

		void show();
		
		void write(unsigned, unsigned, const char*);
		void write_align(string str, char alignment = top|left);
		virtual void draw();

	};
	
	class message_box : public window {
		
		string text;

	public:

		message_box();

		void set_text(string);

		void draw();

	};



class manager_windows {
	vector<window*> items;
public:
	void add(window* w) {
		items.add(w);
	}

	void show() {
		for (unsigned int i = 0; i < items.get_count(); i++) {
			items[i]->show();
		}
	}

	void set_focus(unsigned int n) {
		if (n >= items.get_count()) return;
		window* e = items[n];
		items.remove(n);
		items.add(e);
	}

};

class menu :public window {

protected:

	int select, first, selected_color, selected_bg_color;

public:

	menu();

	struct draw_items_params {
		
		unsigned int x;
		unsigned int y;
		unsigned int row;
	
	};

	//void add(string);

	virtual void oninit();

	virtual bool onkeydown(int);

	virtual void ondrawitem(draw_items_params& params)=0;

	virtual unsigned int get_count() = 0;

	int do_modal();

	void draw();

};

class array_menu :public menu {
protected:
	vector<string> items;
public:
	
	array_menu();

	void add(string s);

	virtual void ondrawitem(draw_items_params&);

	virtual unsigned int get_count();

};

class color_menu :public menu {
public:

	color_menu();

	virtual void ondrawitem(draw_items_params&);

	virtual unsigned int get_count();

};

class files_menu : public array_menu {

	string cur_dir;
	string selected_file;
	int count_dir;

public:

	files_menu();

	string get_selected_file();

	void fill();

	void oninit();

	bool onkeydown(int);

};

}