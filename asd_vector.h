#pragma once

#include <iostream>



namespace asd {

	template<class T>

	class vector {

		T* massive;
		unsigned int count, size, grow;

	public:
		
		vector(unsigned int count=5, unsigned int grow=10);
			
		vector(vector<T>&);

		vector(const std::initializer_list<T>&);

		unsigned int get_count();

		unsigned int count_multiples_of(unsigned int);
		
		bool multiples_of(int, unsigned int);

		vector& add(T);
		
		vector& set_size(unsigned int);

		vector& insert(int, T);

		vector& free_extra();

		vector& remove(unsigned int);

		vector& remove(unsigned int, unsigned int);

		vector& remove_all();

		//int multiple(int, unsigned int);

		double average();

		double get_multi_in(unsigned int, unsigned int);

		vector& def_multi(int, unsigned int);

		vector& def_multiples(int, unsigned int,char);

		vector& operator*=(int);

		vector& operator/=(int);

		vector& operator%=(int);

		vector<T>& operator = (vector<T>&);

		T& operator[](unsigned int);

		~vector();

	};



	///////////////////////////����� ������//////////////////////////////////////



	template<class T>
	vector<T>::vector(unsigned int c, unsigned int g) {
		
		massive = 0;
		count = size = 0;
		grow = g;

		set_size(c);

	}

	template<class T>
	vector<T>::vector(const std::initializer_list<T>& list):
	vector(list.size()){

		for (auto& element : list)massive[count++] = element;

	}

	template<class T>
	vector<T>::vector(vector<T>& obj) {

		massive = 0;
		size = count = obj.count;
		grow = obj.grow;

		set_size(obj.size);
		
		for (int i = 0; i < obj.count; i++)
			massive[i] = obj[i];
		
	}

	template<class T>
	unsigned int vector<T>::get_count() { return count; }

	template<class T>
	unsigned int vector<T>::count_multiples_of(unsigned int number) {
	
		if (number == 0)return 0;;

		unsigned int remember = 0;

		for (unsigned int i = 0; i < count; i++) {

			if (massive[i] == 0)continue;

			if ((int)massive[i] % number == 0)remember++;

		}

		return remember;

	}

	template<class T>
	bool vector<T>::multiples_of(int a, unsigned int b) {

		if (a == 0 || b == 0)return false;

		if (a % b == 0)return true;

		return false;

	}

	template<class T>
	vector<T>& vector<T>::add(T i) {

		if (count >= size)set_size(count + grow);

		massive[count++] = i;

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::set_size(unsigned int number) {

		if (number == 0) {

			if (massive)delete[] massive;
			
			massive = 0;
			count = size = 0;
			
			return *this;

		}

		T* substitution = new T[number];

		if (massive) {

			for (unsigned int i = 0; i<count && i < number; i++)substitution[i] = massive[i];

			delete[] massive;

		}

		massive = substitution;

		if (count > number)count = number;

		size = number;

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::insert(int pos, T obj) {

		if (pos<0 || pos> count) return *this;

		if (count >= size)set_size(count + grow);
		
		T* substitution = new T[count+1];

		for (unsigned int i = 0; i < pos; i++)
			substitution[i] = massive[i];
		
		substitution[pos] = obj;
		
		for (unsigned int i = pos; i < count; i++)
			substitution[i + 1] = massive[i];

		for (unsigned int i = 0; i < count + 1; i++)
			massive[i] = substitution[i];

		count++;

		delete[] substitution;

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::free_extra(){ set_size(count); return *this; }

	template<class T>
	vector<T>& vector<T>::remove(unsigned int pos) { remove(pos, pos); return *this; }

	template<class T>
	vector<T>& vector<T>::remove(unsigned int a, unsigned int b) {

		if (a < 0 || b < a || b >= count) return*this;

		T* substitution = new T[count - (b-a)-1];
		
		int index = 0;

		for (unsigned int i = 0; i < a; i++,index++)
			substitution[index] = massive[i];
		
		for (unsigned int i = b + 1; i < count; i++,index++)
			substitution[index] = massive[i];

		set_size(count - (b - a)-1);

		for (unsigned int i = 0; i < count; i++)
			massive[i] = substitution[i];

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::remove_all(){ set_size(0); return *this; }

	template<class T>
	vector<T>& vector<T>::def_multi(int number, unsigned int control) {

		if (control % 2 == 0)
			for (int i = 1; i < count; i += 2)
				massive[i] *= number;

		else for (int i = 0; i < count; i += 2)
			massive[i] *= number;

		return *this;

	}

	template<class T>
	double vector<T>::average() {

		if (count == 0)return 0;

		double remember =0.;

		for (int i = 0; i < count; i++)
			remember += massive[i];

		if (remember != 0)
			remember /= count;

		return remember;

	}

	template<class T>
	double vector<T>::get_multi_in(unsigned int a, unsigned int b) {

		if (a > b || a<0 || b>=count)return 0;

		double remember = 1.;

		for (; a <= b; a++)
			remember *= massive[a];

		return remember;

	}

	template<class T>
	vector<T>& vector<T>::def_multiples(int number, unsigned control, char key) {

		if (number == 0 || (key != '*' && key!='/' && key!='%'))return *this;

		unsigned int i = 0,plus=2;

		if (control == 0)plus = 1;

		else if (control % 2 == 0)i = 1;
		
		for (; i < count; i += plus) {

			if (massive[i] == 0 && key!='*') continue;

			if(key=='*')
				(int&)massive[i] *= number;

			else if (key == '/')
				(int&)massive[i] /= number;

			else (int&)massive[i] %= number;

		}

		return *this;
				
	}

	template<class T>
	vector<T>& vector<T>::operator*=(int number) {

		for (int i = 0; i < count; i++)
			massive[i] *= number;

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::operator/=(int number) {

		if (number == 0)return *this;

		for (int i = 0; i < count; i++) {

			if (massive[i] == 0)continue;

			massive[i] /= number;

		}

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::operator%=(int number) {

		if (number == 0)return *this;

		int r = 0;

		for (unsigned int i = 0; i < count; i++) {
			
			if (massive[i] == 0)continue;

			(int&)massive[i] %= number;

			(T&)massive[i];

		}

		return *this;

	}

	template<class T>
	vector<T>& vector<T>::operator = (vector<T>& obj) {

		massive = 0;
		size = count = obj.count;
		grow = obj.grow;

		set_size(obj.size);

		for (int i = 0; i < obj.count; i++)
			massive[i] = obj[i];

		return *this;

	}

	template<class T>
	T& vector<T>::operator[](unsigned int i) {
	
		if (i < 0 || i >= count) throw "Error";

		return massive[i];

	}

	template<class T>
	vector<T>::~vector() { set_size(0); }

}