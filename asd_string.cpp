#include <iostream>
#include "read.h"
#include "asd_string.h"
#include "sorts.h"


#pragma warning(disable:4996)

asd::string::string(const char* str) {

	this->str = 0;
	size = 0;
	add(str);

}

asd::string::string(string& obj) {

	this->str = 0;
	size = 0;
	add(obj.str);

}

asd::string::string(char letter, unsigned int count) {

	str = 0;
	size = 0;

	if (count > 0) {

		str = new char[(unsigned long long)count + 1];

		for (unsigned int i = 0; i < count; i++)str[i] = letter;

		str[count] = 0;

		size = count;			

	}

}

asd::string& asd::string::add(const char* str) {

	if (!str) return *this;

	unsigned long long new_size = size + strlen(str) + 1;

	char* new_str = new char[new_size];
	new_str[0] = 0;

	if (this->str) strcpy_s(new_str, new_size, this->str);
	strcat_s(new_str, new_size, str);

	delete this->str;

	this->str = new_str;
	size = new_size - 1;

	return *this;
}

unsigned int asd::string::lenght() { return size; }

char * asd::string::get_longest_word() {

	int len = 0, max_len=0;

	char* str = strtok((char*)c_str(), " ");

	char* word = str;

	while (str != 0) {

		for (; str[len]; len++);

		if (len > max_len) { max_len = len; word = str; }

		len = 0;

		str = strtok(0, " ");


	}

	return word;
	

}

void asd::string::empty() {

	if (str)delete str;
	str = 0;
	size = 0;

}

unsigned int asd::string::count_letters() {

	unsigned int count = 0;

	for (unsigned int i = 0; i < size; i++)
		if (my_isalpha(str[i]))
			count++;
	
	return count;

}

unsigned int asd::string::count_letters(char key) {

	unsigned int count = 0;

	for (unsigned int i = 0; i < size; i++)
		if (str[i] == key)count++;

	return count;

}

unsigned int asd::string::count_words() {

	asd::string m = *this;

	unsigned int count = 0;

	bool word = false;

	m.remove_extra_spaces();

	for (unsigned int i = 0; i < size; i++) {

		if (m[i] != ' ' && !word) { count++; word = true; }

		else if (m[i] == ' ' && word)word = false;

	}
		
	return count;

}

unsigned int asd::string::count_words(const char * word) {

	asd::string m = *this;

	unsigned int count = 0;

	bool protect = false;

	unsigned long long len = strlen(word);
	
	m.remove_extra_spaces();

	for (unsigned int i = 0,j=0; i < size; i++) {

		if (!protect && m[i] == word[j])j++;

		else if (m[i] != word[j]) { j = 0; protect = true; }

		if (m[i] == ' ') { protect = false; j = 0; }

		if (len == j && m[i+1]==' ') { count++; j = 0; }

	}

	return count;

}

unsigned int asd::string::count_digits() {

	unsigned int count = 0;

	for (unsigned int i = 0; i < size; i++)
		if (my_isdigit(str[i]))
			count++;

	return count;

}

unsigned int asd::string::count_numbers() {

	if (!is_digit_string())return 0;
	
	asd::string sub = *this;

	sub.remove_extra_spaces();

	bool number = false; int count = 0;

	for (int i = 0; i < size; i++) {

		if ((isdigit(str[i]) || str[i] == '-') && number == false) { number = true; count++; }

		else if (str[i] == ' ')number = false;

	}

	return count;

}

unsigned int asd::string::count_substr(const char * word) {
	
	unsigned long long count = 0,j=0,len = strlen(word);

	for (unsigned int i = 0; i < size; i++) {

		if (str[i] == word[j])j++;

		else if (j && str[i] != word[j]) { j = 0; i--; }

		else continue;

		if (j == len) { count++; j = 0; }

	}

	return count;

}

void asd::string::sort(int param) {
	
	if (size <= 1)return; 
	
	if (param > 0) quick_sort(str, 0, size - 1);

	else reverse_radix_sort(str, size);

}

void asd::string::sort_numbers(int param) {

	unsigned int count = count_digits();

	if (count <= 1)return;

	char* substitution = new char[(unsigned long long)count+ 1];

	substitution[count] = 0;

	int j = 0;

	for (unsigned int i = 0; i < size; i++)
		if (my_isdigit(str[i]))
			substitution[j++] = str[i];

	quick_sort(substitution, 0, count-1);

	j = 0;


	if (param < 0) {

		for (int i = (int)size - 1; i >= 0; i--)
			if (my_isdigit(str[i]))
				str[i] = substitution[j++];

	}

	else for (unsigned int i = 0; i < size; i++)
			if (my_isdigit(str[i]))
				str[i] = substitution[j++];

	delete[] substitution;

}

void asd::string::sort_letters(int param) {

	unsigned int count = count_letters();

	if (count <= 1)return;

	char* substitution = new char[(unsigned long long)count + 1];

	substitution[count] = 0;

	unsigned int j = 0;

	for (unsigned int i = 0; i < size; i++)
		if (my_isalpha(str[i]))
			substitution[j++] = str[i];

	quick_sort(substitution, 0, count - 1);

	j = 0;


	if (param < 0) {

		for (int i = (int)size - 1; i >= 0; i--)
			if (my_isalpha(str[i]))
				str[i] = substitution[j++];

	}

	else for (unsigned int i = 0; i < size; i++)
		if (my_isalpha(str[i]))
			str[i] = substitution[j++];

	delete[] substitution;

}

const char* asd::string::c_str() { return ((str) ? str : ""); }

int asd::string::compare(const char* str) { return strcmp(this->str, str); }

asd::string& asd::string::set(const char* str) {

	empty();
	return add(str);

}

asd::string& asd::string::set(string& obj) {

	empty();
	return add(obj.str);

}

int asd::string::find(char letter, int number) {

	int i = 0;
	
	bool find = false;

	if (number < 0) {

		for (i = (int)size - 1; i >= 0; i--)
			if (str[i] == letter) {
				
				find = true;
				break;

			}

	}

	else for (; i < (int)size; i++)
		if (str[i] == letter) {

			find = true;
			break;

		}

	return ((find) ? i : -1);

}

int asd::string::find_substr(const char* word, int number) {

	unsigned int len = strlen(word), j = 0,protect = 0;

	int i = 0;

	bool find = false;

	if (number < 0)
		for (i = (int)size - 1, j = len - 1; i >= 0; i--) {
			
			if (str[i] == word[j]) { protect++; j--; }

			else if (protect && word[i] != str[j]) { protect = 0; j = len - 1; i++; }

			else continue;

			if (protect == len) { find = true; break; }

		}

	else for (; i < (int)size; i++) {

		if (str[i] == word[j]) { protect++; j++; }

		else if (protect && word[j] != str[i]) { protect = 0; j = 0; i--; }

		else continue;

		if (protect == len) { find = true; i -= len - 1; break; }

	}


	return ((find)?i:-1);
}

asd::string& asd::string::insert(unsigned pos, char key) {

	if (pos > size) return *this;
	
	unsigned int len = size;
	
	char* str1 = new char[(unsigned long long)len + 2];
	
	for (unsigned int i = 0; i <= len; i++) {
		
		if (i < pos) str1[i] = str[i];

		else if (i > pos) str1[i] = str[i - 1];

		else str1[i] = key;
	
	}

	str1[len + 1] = 0;
	
	delete str;
	str = str1;
	size++;
	return *this;

}

asd::string& asd::string::insert(unsigned i, const char* _str) {

	if (strlen(_str) == 0 || i > size)return *this;;

	unsigned int z = 0;
	char* substitution = new char[size + strlen(_str) + 1];

	substitution[size + strlen(_str)] = 0;

	for (unsigned int j = 0; j < i; j++,z++)
		substitution[z] = str[j];

	for (unsigned int j = 0; _str[j] != 0; j++, z++)
		substitution[z] = _str[j];

	for (unsigned int j = i; j < size; j++, z++)
		substitution[z] = str[j];

	*this = substitution;
	
	delete[] substitution;

	return *this;

}

asd::string& asd::string::insert(unsigned i, string& obj) {

	insert(i, obj.str);
	return *this;

}

asd::string& asd::string::insert_after_char(char letter, const char* word) {

	if (count_letters(letter) == 0) return *this;

	unsigned int len = strlen(word);

	for(unsigned int i =0; i< size; i++)
		if (str[i] == letter) {

			insert(++i, word);
			i += len;

		}

	return *this;

}

bool asd::string::is_digit_string() {

	string sub = *this;

	sub.remove_extra_spaces();

	for (unsigned int i = 0; i < sub.size; i++)
		if (sub.str[i] != ' ' && !my_isdigit(sub.str[i]) && sub.str[i] != '-')return false;

	char* str = strtok(sub.str, " ");

	while (str != 0) {

		if (!is_numeric(str, 0))
			return false;

		str = strtok(0, " ");

	}

	return true;

}

bool asd::string::is_float_string() {

	string sub = *this;

	sub.remove_extra_spaces();

	for (unsigned int i = 0; i < sub.size; i++)
		if (sub.str[i] != ' ' && !my_isdigit(sub.str[i]) && sub.str[i] != '.' && sub.str[i] != '-')return false;

	char* str = strtok(sub.str, " ");

	while (str != 0) {

		if (!is_numeric(str, 1))
			return false;

		str = strtok(0, " ");

	}

	return true;

}

asd::vector<int>& asd::string::to_digit(vector<int>& obj) {

	if (!is_digit_string())return obj;

	asd::string sub = *this;

	char* str = strtok(sub.str, " ");
	
	while (str != 0) {

		obj.add(atoi(str));

		str = strtok(0, " ");

	}

	return obj;

}

asd::vector<float>& asd::string::to_float(vector<float>& obj) {

	if (!is_float_string())return obj;

	asd::string sub = *this;

	char* str = strtok(sub.str, " ");
	
	while (str != 0) {

		 obj.add((double)atof(str));

		str = strtok(0, " ");

	}

	return obj;

}

asd::string& asd::string::to_string(int number) {

	char str[10] = { 0 };

	_itoa_s(number, str, 10);

	add(str);

	return *this;

}

asd::string& asd::string::replace_letters(char key, char replace) {

	if (key != replace)
		for (unsigned int i = 0; i < size; i++)
			if (str[i] == key)
				str[i] = replace;

	return *this;

}

asd::string& asd::string::replace_words(const char* word, const char* replace) {

	if (strlen(word) == 0 || strlen(replace) == 0 || find_substr(word, 1)==-1)return *this;

	unsigned int len = strlen(word), len_rep = strlen(replace), count = 0, protect=0;

	for (unsigned int i = 0,j=0; i < size; i++) {

		if (str[i] == ' ') {

			j = protect = count = 0;
			continue;

		}

		else if (str[i] != ' ') {
		
			if (str[i] == word[j])count++;
			j++;
			protect++;

		}

		if (count==protect && len==protect && (str[i+1]==' ' || str[i+1]==0)) {

			i -= len-1;

			remove_in(i, i + len-1);

			insert(i, replace);

			i += len_rep-1;

			count = 0;

		}
		
	}

	return *this;

}

asd::string& asd::string::replace_substrs(const char* word, const char* replace) {

	if (strlen(word) == 0 || strlen(replace) == 0 || find_substr(word, 1)==-1)return *this;

	unsigned int len_word = strlen(word), len_rep = strlen(replace);

	for (unsigned int i = 0,j=0; i < size; i++) {

		if (str[i] == word[j])j++;

		else if (str[i] != word[j] && j) { j = 0; i--; }

		else continue;

		if (j == len_word) {

			i -= len_word - 1;

			remove_in(i, i + len_word - 1);

			insert(i, replace);

			i += len_rep - 1;

			j = 0;

		}

	}

	return *this;

}

asd::string& asd::string::swap_letter(unsigned int key, unsigned int replace) {

	if (key > size || replace > size)return *this;

	char sub = str[key];
	str[key] = str[replace];
	str[replace] = sub;

	return *this;

}

asd::string& asd::string::swap_substr(const char* word_1, const char* word_2, unsigned i, unsigned j) {

	if (strlen(word_1) == 0 || strlen(word_2) == 0 ||
		count_substr(word_1) < i || count_substr(word_2) < j || !i || !j)
		return *this;

	unsigned help_i = 0, help_j = 0, len_1 = strlen(word_1), len_2 = strlen(word_2), z, x, ii, jj;

	bool protect = false;

	z = x = ii = jj = 0;

	for (; z < size; z++) {

		if (str[z] == word_1[ii])ii++;

		else if (str[z] != word_1[ii] && ii) { ii = 0; z--; }
		
		if (ii == len_1) { help_i++; ii = 0; }

		if (help_i == i)break;

	}

	for (; x < size; x++) {

		if (str[x] == word_2[jj])jj++;

		else if (str[x] != word_1[jj] && jj) { jj = 0; x--; }

		if (jj == len_2) { help_j++; jj = 0; }

		if (help_j == j)break;

	}

	z -= len_1 - 1;
	x -= len_2 - 1;

	remove_in(z, z+len_1 - 1);
	insert(z, word_2);

	x += len_2-len_1;
	
	remove_in(x, x + len_2 - 1);
	insert(x, word_1);

	return *this;
}

asd::string& asd::string::reverse() {

	char* substitution = new char[(unsigned long long)size + 1];

	int j = 0;

	for (int i = size - 1; i >= 0; i--, j++) 
		substitution[j] = str[i];

	
	substitution[j] = 0;

	*this = substitution;

	delete[] substitution;

	return *this;

}

asd::string& asd::string::remove_extra_spaces() {

	char* substitution = new char[(unsigned long long)size+1];

	unsigned int  j = 0;

	bool first_letter = false;

	for (unsigned int i = 0; i < size; i++) {

		if (first_letter == false && str[i] == ' ')continue;

		else if (str[i] != ' ' && first_letter == false)first_letter = true;

		if ((str[i] == ' ' && str[i + 1] == ' ') || (i == size - 1 && str[i] == ' '))
			continue;

		substitution[j] = str[i];
		j++;

	}

	substitution[j] = 0;

	*this = substitution;

	delete[] substitution;

	return *this;

}

asd::vector<asd::string>& asd::string::explode(const char* words,vector<string>& obj) {

	obj.remove_all();

	asd::string sub = *this;

	char* str = strtok(sub.str, words);
	
	while (str != 0) {

		obj.add(str);
		
		str = strtok(0, words);

	}

	return obj;

}

asd::string& asd::string::remove_in(unsigned i) { return remove_in(i, i); }

asd::string& asd::string::remove_in(unsigned a, unsigned b) {

	if (a >= size || b >= size || a > b) return *this;

	char* substitution = new char[(unsigned long long)size-(b-a)+1];

	unsigned int j = 0;

	for (unsigned int i = 0; i < a; i++,j++) substitution[j] = str[i];

	for (unsigned int i = b + 1; i < size; i++,j++) substitution[j] = str[i];

	substitution[j] = 0;

	*this = substitution;

	delete[] substitution;

	return *this;

}

asd::string& asd::string::remove_from_to(char from, char to) {

	if (size < 2 || find(from, 1)==-1) return *this;
	
	int control_delete = -1;

	for (int i = 0; i < size; i++) {

		if (control_delete == -1 && str[i] == from)
			control_delete = i;

		else if (control_delete != -1 && str[i] == to) {
			
			remove_in(control_delete, i);
			i = control_delete = -1;

		}

	}

	if (control_delete != -1)
		remove_in(control_delete, size-1);

	return *this;

}

asd::string& asd::string::operator=(const char* str) { return set(str); }

asd::string& asd::string::operator=(string& obj) { return set(obj.str); }

asd::string& asd::string::operator+=(const char* str) { add(str); return *this; }

asd::string& asd::string::operator+=(string& obj) { add(obj.str); return *this; }

asd::string& asd::string::operator+=(char letter) { return *this += string(letter, 1); }

bool asd::string::operator==(const char* str) { return !compare(str); }

bool asd::string::operator==(string& obj) {	return !compare(obj.str); }

bool asd::string::operator!=(const char* str) { return compare(str) != 0; }

bool asd::string::operator!=(string& obj) { return compare(obj.str) != 0; }

bool asd::string::operator!() {	return size == 0; }

asd::string asd::string::operator+(const char* str) { return asd::string(this->str).add(str); }

asd::string asd::string::operator+(string& obj) { return asd::string(str).add(obj.str); }

bool asd::string::operator>(const char* str) { 	return compare(str) > 0; }

bool asd::string::operator>(string& obj) { return compare(obj.str) > 0; }

bool asd::string::operator<(const char* str) { return compare(str) < 0; }

bool asd::string::operator<(string& obj) { return compare(obj.str) < 0; }

bool asd::string::operator<=(const char* str) {	return compare(str) <= 0; }

bool asd::string::operator<=(string& obj) {	return compare(obj.str) <= 0; }

bool asd::string::operator>=(const char* str) {	return compare(str) >= 0; }

bool asd::string::operator>=(string& obj) {	return compare(obj.str) >= 0; }

double asd::string::operator%(char word) {

	if (!asd::my_isalpha(word)) return 0;

	int len = lenght();

	int kol = 0;

	for (int i = 0; i < len; i++)
		if (str[i] == word)kol++;

	return (kol * 100.) / len;

}

char& asd::string::operator[](unsigned int i) {

	if (i >= size) throw std::out_of_range("out_of_array");

	return str[i];

}

asd::string::operator char* () { return (char*)c_str(); }

void asd::string::operator()() { empty(); }

asd::string& asd::string::operator()(const char* str) {

	return	set(str);

}

asd::string& asd::string::operator()(string& obj) {

	return set(obj.str);

}

asd::string& asd::string::operator<<(const char* str) {

	return add(str);

}

asd::string& asd::string::operator<<(string& obj) {

	return add(obj.str);

}

std::ostream& operator <<(std::ostream& obj, asd::string& str) {

	obj << str.c_str();

	return obj;

}

asd::string operator + (const char* str, asd::string& obj) {

	return asd::string(str).add(obj);

}

asd::string& operator +=(const char* str, asd::string& obj) {

	return asd::string(obj).add(str);

}