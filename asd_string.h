#pragma once

#include <iostream>
#include "asd_vector.h"


namespace asd {

	using std::cout;
	using std::endl;

	class string {

		char* str;
		unsigned int size;

	public:

		string(const char* str = 0);
		string(string&);
		string(char, unsigned int);

		unsigned int lenght();

		char* get_longest_word();

		unsigned int count_letters();

		unsigned int count_letters(char);

		unsigned int count_words();

		unsigned int count_words(const char*);

		unsigned int count_digits();

		unsigned int count_numbers();

		unsigned int count_substr(const char *);

		void sort(int);

		void sort_numbers(int);

		void sort_letters(int);

		void empty();

		const char* c_str();

		int compare(const char*);//������ � �������

		string& add(const char*);

		string& set(const char*);

		string& set(string&);

		int find(char, int);

		int find_substr(const char*, int i = 1);

		string& insert(unsigned, char);

		string& insert(unsigned, const char*);

		string& insert(unsigned, string&);

		string& insert_after_char(char, const char*);

		bool is_digit_string();
	
		bool is_float_string();

		vector<int>& to_digit(vector<int>&);

		vector<float>& to_float(vector<float>&);

		string& to_string(int);

	//	string& insert_after_substr(const char *, const char*);

		string& replace_letters(char, char);

		string& replace_words(const char*, const char*);

		string& replace_substrs(const char*, const char*);

		string& swap_letter(unsigned int, unsigned int);

		string& swap_substr(const char*, const char*, unsigned i = 1, unsigned j = 1);

		string& reverse();

		string& remove_extra_spaces();

		vector<string>& explode(const char*, vector<string>&);

		string& remove_in(unsigned);

		string& remove_in(unsigned, unsigned);

	    string& remove_from_to(char,char);

		string& operator =(const char*);

		string& operator =(string&);

		string& operator +=(const char*);

		string& operator +=(char);

		string& operator +=(string&);

		bool operator ==(const char*);

		bool operator ==(string&);

		bool operator !=(const char*);

		bool operator !=(string&);

		bool operator !();

		string operator +(const char*);

		string operator +(string&);

		bool operator >(const char*);

		bool operator >(string&);

		bool operator <(const char*);

		bool operator <(string&);

		bool operator >=(const char*);

		bool operator >=(string&);

		bool operator <=(const char*);

		bool operator <=(string&);

		double operator %(char);

		char& operator[](unsigned int i);

		operator char* ();

		void operator () ();

		string& operator () (const char*);

		string& operator () (string&);

		string& operator <<(const char*);

		string& operator <<(string&);

		~string() { empty(); }

	};

}

std::ostream& operator <<(std::ostream&, asd::string&);

asd::string operator + (const char*, asd::string&);

asd::string& operator +=(const char*, asd::string&);