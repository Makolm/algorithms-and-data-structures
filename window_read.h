#pragma once

#include "asd_all.h"

namespace asd {

	class window_read{

		unsigned user_width, user_height,info_x,info_x_border,info_y, wnd_y;

		window for_user;
		window wnd_success;

		vector<int> vec_int;
		vector<vector<int>> vec_vec;

		void read_pos(unsigned,unsigned,unsigned,read&);

	public:

		enum methods_for_string {

			usually_string,
			percent_letter,
			replace_letter,
			count_words_space,
			reverse,
			replace_words,
			find_letter_digit,
			sorts

		};

		enum methods_for_vectors {

			usually_vectors,
			DMD_of_vector,

		};

		enum methods_for_vec2 {

			multiple_matrix,

		};
		
		window_read();

		void write_str(methods_for_string);

		void write_vec(methods_for_vectors,bool);

		void show();

		void show_info(string, methods_for_string);

		void show_info(methods_for_vectors,unsigned);

	};

}