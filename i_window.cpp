#include <iostream>
#include <conio.h>
#include <io.h>
#include "asd_all.h"

namespace asd {

	window::window() {

		x = y = 0;

		w = get_cols();
		h = get_rows()-1;
		
		border = 1;

		color = blue;
		bg_color = dark_gray;
		border_color = blue;
		
		buf[0] = 218;
		buf[1] = 196;
		buf[2] = 191;
		buf[3] = 179;
		buf[4] = 192;
		buf[5] = 217;

	}

	unsigned int window::get_client_width() { return w - get_border() * 2; }
	
	unsigned int window::get_client_height() { return h - get_border() * 2; }
	
	unsigned int window::get_border() { return border; }

	unsigned int window::get_x() { return x; }

	unsigned int window::get_y() { return y; }

	void window::set_color(unsigned int c) {color = c &= 16; }

	void window::set_bg_color(unsigned int c){ bg_color = c % 16; }

	void window::set_border_color(unsigned int c) { border_color = c % 16; }

	void window::clear() {

		go_to(0,0);

		::set_color(bg_color, bg_color);

		for (unsigned int i = 0; i < h-2; i++) {
			for (unsigned int j = 0; j < w-2; j++)
				if (i != 0 && j != 0)std::cout << " ";

			go_to(x+1, y+i+1);
		}

		go_to(x + 1, y + 1);

		::set_color(color, bg_color);

	}

	void window::move(unsigned int x_, unsigned int y_) { x = x_; y = y_; }

	void window::set_size(unsigned int w_, unsigned int h_) { w = w_, h = h_; }

	void window::set_border(unsigned int bor) { border = bor; }

	void window::set_align(char alignment) {

		if ((alignment & center) && w < ::get_cols())
			x = (::get_cols() - w) / 2;

		else if ((alignment & right) && w < ::get_cols())
			x = ::get_cols() - w;

		else if (alignment & left)
			x = 0;

		if ((alignment & middle) && h < ::get_rows())
			y = (::get_rows() - h) / 2;

		else if ((alignment & bottom) && h < ::get_rows())
			y = ::get_rows() - h;

		else if (alignment & top)
			y = 0;

	}

	void window::show() {

		go_to(x, y);

		::set_color(border_color, bg_color);
		
		std::cout << buf[0];
		for (unsigned int i = 0; i < w - 2; i++) std::cout << buf[1];
		std::cout << buf[2];

		for (unsigned int i = 0; i < h - 2; i++) {
			
			go_to(x, y + i + 1);
			
			std::cout << buf[3];
			::set_color(bg_color, bg_color);
			
			for (unsigned int j = 0; j < w - 2; j++) std::cout << " ";
			
			::set_color(border_color, bg_color);
			std::cout << buf[3];
		}

		go_to(x, y + h-1);

		std::cout << buf[4];
		for (unsigned int i = 0; i < w - 2; i++) std::cout << buf[1];
		std::cout << buf[5];

		go_to(x + 1, y + 1);

		draw();
	}

	void window::write(unsigned x_, unsigned y_, const char* buff) {

		if (x_ >= get_client_width() || y_ >= get_client_height())return;

		go_to(x + x_ + get_border(), y + y_ + get_border());

		unsigned int sub = get_client_width();

		for (unsigned int i = 0; x_ + i < sub && buff[i]; i++)
			std::cout << buff[i];
		
		std::cout << std::flush;

	}

	void window::write_align(string buff, char alignment) {

		unsigned int text_x = 0, text_y = 0, sub = get_client_width();

		if (sub < 1 && get_client_height() < 1)return;

		if (alignment & center)text_x = (sub - buff.lenght()) / 2;

		else if (alignment & right)text_x = sub - buff.lenght();

		else if (alignment & left)text_x = 0;

		if (alignment & middle)text_y = get_client_height() / 2;

		else if (alignment & bottom)text_y = get_client_height() - 1;

		else if (alignment & top)text_y = 0;

		if (buff.lenght() >= sub)text_x = 0;

		::go_to(x + text_x + get_border(), y + text_y + get_border());

		for (unsigned int i = 0; i < sub && i < buff.lenght(); i++)std::cout << buff[i];

		std::cout << std::flush;

	}

	void window::draw(){}

	message_box::message_box() :window() {
	
		set_size(20, 40);
		move(30, 12);
	
	}

	void message_box::set_text(string text) { this->text = text; }

	void message_box::draw() { cout << text << endl; }

	menu::menu() {

		first = 0;
		select = 0;
		selected_color = blue;
		selected_bg_color = cyan;

	}


	void menu::oninit() { ; }

	bool menu::onkeydown(int) { return false; }

	int menu::do_modal() {

		first = 0;
		oninit();

		while (1) {

			if (select > first + (int)get_client_height() - 1)
				first = select - (int)get_client_height() + 1;

			else if (select < first)
				first = select;

			show();
			int key = _getch();

			if (key == 224) {

				key = _getch();

				if (onkeydown(key))continue;

				else if (key == 80) {

					select++;
					if (select == get_count())select = 0;

				}

				else if (key == 72 && select > 0) select--;

			}

			else if (onkeydown(key))continue;

			else if (key == 27) {

				select = -1;
				break;

			}

			else if (key == 13)break;

		}

		return select;
	}

	void menu::draw() {

		draw_items_params params;
		for (unsigned int i = first, y = 0; i < get_count() && y < get_client_height(); i++, y++) {
			
			if (i == select) {
				::set_color(selected_color, selected_bg_color);
			}
			
			else {
				::set_color(color, bg_color);
			}
			
			params.x = x + 1;
			params.y = this->y + y + 1;
			params.row = i;
			
			ondrawitem(params);

		}
	}

	array_menu::array_menu() : menu() {}

	void array_menu::add(string s) { items.add(s); }

	void array_menu::ondrawitem(draw_items_params& params) {

		::go_to(params.x, params.y);

		for (unsigned int j = 0; j < get_client_width(); j++) {

			if (j < items[params.row].lenght()) 
				std::cout << items[params.row][j];

			else {
				std::cout << " ";
			}
		}

	}

	unsigned int array_menu::get_count() { return items.get_count(); };

	color_menu::color_menu():menu() { ; }

	void color_menu::ondrawitem(draw_items_params& params){
	
		::set_color(color, bg_color);
		::go_to(params.x, params.y);
		if (params.row == select) {
			std::cout << ">";
		}
		else {
			std::cout << " ";
		}
		::go_to(params.x + get_client_width() - 1, params.y);
		if (params.row == select) {
			std::cout << "<";
		}
		else {
			std::cout << " ";
		}
		::go_to(params.x + 1, params.y);
		for (unsigned int j = 1; j < get_client_width() - 1; j++) {
			::set_color(params.row, params.row);
			std::cout << " ";
		}

	}

	unsigned int color_menu::get_count() { return 16; }

	files_menu::files_menu() : array_menu() { count_dir = 0; }

	string files_menu::get_selected_file() { return selected_file; }

	void files_menu::fill() {

		count_dir = 0;

		items.remove_all();

		string tmp(cur_dir);
		tmp += "\\*.*";
		
		_finddata_t fileinfo;
		intptr_t list = _findfirst(tmp, &fileinfo);

		while (1) {

			if (strcmp(fileinfo.name, ".") && fileinfo.attrib & _A_SUBDIR) {

				items.add(fileinfo.name);
				count_dir++;

			}

			if (_findnext(list, &fileinfo) < 0)break;

		}

		_findclose(list);

		list = _findfirst(tmp, &fileinfo);

		while (1) {

			if (!(fileinfo.attrib & _A_SUBDIR))items.add(fileinfo.name);

			if (_findnext(list, &fileinfo) < 0)break;

		}

		_findclose(list);

	}

	void files_menu::oninit() {

		char buff[MAX_PATH + 1];
		GetCurrentDirectoryA(MAX_PATH, buff);
		cur_dir = buff;
		fill();

	}

	bool files_menu::onkeydown(int key) {

		if (key == 13) {

			if (select >= count_dir) {

				selected_file = cur_dir + "\\" + items[select];
				return false;

			}

			else if (items[select] == "..") {

				for (int i = cur_dir.lenght()-1; i >= 0; i--) {
					
					if (cur_dir[i] == '\\') {

						cur_dir[i] = 0;
						break;

					}

				}

				select = 0;
				fill();

				return true;

			}

			else {

				cur_dir += "\\" + items[select];
				select = 0;
				fill();

				return true;

			}

		}

		return false;

	}

}

