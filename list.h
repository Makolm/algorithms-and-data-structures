#pragma once



namespace asd {
	
	template<class T>
	class list {

		class node {
		public:

			T data;
			node* next;
			node* back;

			node(T, node*, node *n=0);

			node * get() { return this; }

		};

	
		int count;
		node* head, * tail;

	public:

		typedef node* pos;

		list();

		int get_count();

		T& get_next(pos&);

		T& get_back(pos&);

		pos get_head() { return (pos)head; }

		pos get_tail() { return (pos)tail; }

		list& add_head(T);

		list& add_tail(T);

		pos& find(T sub) {

			node* substitution = head;

			while (substitution) {

				if (substitution->data == sub)break;

				substitution = substitution->next;

			}

			return (pos&)substitution;


		}

		void insert(T,T);

		void remove(T);

		void remove_all();

		list& operator*=(int);

		list& operator/=(int);

		list& operator%=(int);

		~list();

	};


	template<class T>
	list<T>::list() { head = tail = 0; count = 0; }

	template<class T>
	list<T>::node::node(T data, node* p, node* n) {

		this->data = data;
		back = p; next = n;

	}

	template<class T>
	int list<T>::get_count(){ return count; }

	template<class T>
	T& list<T>::get_next(pos& type) {

		T& data = ((node*)type)->data;
		type = (pos)((node*)type)->next;
		return data;

	}

	template<class T>
	T& list<T>::get_back(pos&type) {

		T& data = ((node*)type)->data;
		type = (pos)((node*)type)->back;
		return data;

	}
	
	template<class T>
	list<T>& list<T>::add_head(T data) {

		node* new_node = new node(data, 0, head);

		if (head)head->back = new_node;
		if (!tail)tail = new_node;

		head = new_node;
		count++;

		return *this;
	}

	template<class T>
	list<T>& list<T>::add_tail(T data) {

		node* new_node = new node(data, tail);
		
		if (tail)tail->next = new_node;
		if(!head)head = new_node;
		
		tail = new_node;
		count++;

		return *this;

	}

	template<class T>
	void list<T>::insert(T data,T data2) {

		node* substitution = find(data);

		if (!substitution) return;

		node* new_node = new node(data2, substitution, substitution->next);

		substitution->next = new_node;
		
		if (substitution == tail)tail = new_node;

		else (new_node->next)->back = new_node;

		count++;

	}

	template<class T>
	void list<T>::remove(T data) {

		node* substitution = (node*)find(data);

		if (!substitution) return;

		if (count == 1) {
	
			head = 0;
			tail = 0;
			delete substitution;

		}

		else if (substitution == head) {
			(head->next)->back = 0;
			head = head->next; 
			delete substitution; 
		}

		else if (substitution == tail) {

			(tail->back)->next = 0;
			tail = tail->back;
			delete substitution;

		}

		else {

			(substitution->next)->back = substitution->back;
			(substitution->back)->next = substitution->next;
			delete substitution;

		}

		count--;

	}

	template<class T>
	void list<T>::remove_all() {

		node* substitution = head;

		while (substitution) {
			
			node* sub = substitution;
			substitution = substitution->next;
			delete sub;

		}

	}

	template<class T>
	list<T>& list<T>::operator*=(int number) {
			
		pos ptr = get_head();

		while (ptr != 0)
			get_next(ptr) *= number;

		return *this;

	}

	template<class T>
	list<T>& list<T>::operator/=(int number) {

		if (number == 0)return *this;

		pos ptr = get_head();

		while (ptr != 0) {

			T& m = get_next(ptr);

			if (m != 0)m /= number;

		}

		return *this;

	}

	template<class T>
	list<T>& list<T>::operator%=(int number) {

		if (number == 0)return *this;

		pos ptr = get_head();

		while (ptr != 0) {

			int& m = (int&)get_next(ptr);

			if (m != 0)m %= number; 

		}

		return *this;

	}

	template<class T>
	list<T>::~list() { remove_all(); }

}