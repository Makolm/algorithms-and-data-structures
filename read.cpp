#include <iostream>
#include <conio.h>
#include "read.h"
#include "asd_string.h"
#include "fun_console.h"

namespace asd {

	bool read::text(methods_read method) {

		unsigned int  pos, old_len;
		pos = old_len = str.lenght();

		while (1) {

			go_to(x, y);

			if (old_len > str.lenght())
				for (unsigned int  i = 0; i < old_len; i++)
					std::cout << " ";

			old_len = str.lenght();

			old_str = str;

			go_to(x, y);
			std::cout << str;
			go_to(x + pos, y);

			int key = _getch();

			if (key == 224) {

				key = _getch();

				if (key == 75 && pos) {//left
					pos--;
				}
				else if (key == 77 && pos < str.lenght()) {//right
					pos++;
				}
				else if (key == 83 && pos < str.lenght()) {//del
					str.remove_in((pos == str.lenght() - 1) ? pos : (pos + 1));
				}
				continue;

			}

			else if (key == 27) { 
				
				str = ""; 
				go_to(x, y);
				for (unsigned int  i = 0; i < old_len; i++)
					std::cout << " ";
			
				return false; 
			
			}

			else if (key == 13) {


				go_to(x, y);
				std::cout << str;

				return true;
				
			}

			else if (key == 8 && pos)  str.remove_in(--pos);

			else if (key >= 32 && (!lenght || str.lenght() < lenght)) {

				if (method != usually && !for_user(method, pos,char(key)))
					continue;
			
				str.insert(pos, (char)key);
				pos++;

			}

		}

	}

	bool read::for_user(methods_read method, int pos, char key) {

		old_str.insert(pos, key);

		int len = old_str.lenght();

		switch (method) {

			case only_letters: {

				for (int i = 0; i < len; i++)
					if (!my_isalpha(old_str[i])) return false;//isalpha() �� ��������� ��������

				return true;
			}

			case only_digits: {

				for (int i = 0; i < len; i++)
					if (!my_isdigit(old_str[i]))return false;

				return true;

			}

			case letters_space: {

				for (int i = 0; i < len; i++)
					if (!my_isalpha(old_str[i]) && old_str[i]!=' ') return false;
				//isalpha() �� ��������� ��������

				return true;
			}

			case letters_digits: {

				for (int i = 0; i < len; i++)
					if (!my_isalpha(old_str[i]) && !my_isdigit(old_str[i])) return false;
				//isalpha() �� ��������� ��������

				return true;
			}

			case letters_digits_space: {

				for (int i = 0; i < len; i++)
					if (!my_isalpha(old_str[i]) && !my_isdigit(old_str[i]) && old_str[i] != ' ')
						return false;

				return true;

			}

			case digits_space: {

				if (!old_str.is_digit_string()) return false;

				return true;

			}

			case floats_space: {

				if(!old_str.is_float_string()) return false;

				return true;

			}

		}

		return false;

	}

	bool my_isalpha(char key) {

		if ((key < 'A' || key > 'Z') && (key < 'a' || key > 'z'))
			return false;
		
		return true;

	}

	bool my_isdigit(char key) {

		if (key < '0' || key > '9')
			return false;

		return true;

	}

	bool is_numeric(char* str, bool dr)	{
		
		int i, len = strlen(str);
		
		if (!len || str[0] == '.')return false;
		
		for (i = 0; i < len; i++)
		{
			if (i == 0 && (str[0] == '+' || str[0] == '-')) continue;
		
			if ((str[i] >= '0' && str[i] <= '9') ||
				str[i] == '.')  continue;
			
			return false;
		
		}
		
		int pos = 0;
		
		for (i = 0; i < len; i++)
			if (str[i] == '.')pos++;
		
		if ((!dr && pos) || (pos > 1)) return false;
		
		return true;
	}

}

	/*
}

//0 - ������ �����
//9 - ����� �����
//L - ������ �����
//? - ����� �����
bool read::validate(char key, int pos, const char* text_) {

		old_str.insert(pos, key);

		int i = 0, j = 0;
		int protect = 0, limit = 0;

		int str_len = old_str.lenght(), val_len = strlen(text_);

		if (str_len > val_len)return true;

		while ( i < val_len && j < str_len) {

			protect=0,limit=0;

			if (text_[i] == '0') {

				for (; text_[i] == '0' && i < val_len; i++, limit++);

				for (; isdigit(old_str[j]) && j < str_len && protect<limit; j++, protect++);

				if (old_str[j] != 0 && protect < limit)return true;
			}

			else if (text_[i] == 'L') {

				for (; text_[i] == 'L' && i < val_len; i++, limit++);

				for (; isalpha(old_str[j]) && j < str_len && protect < limit; j++, protect++);

				if (old_str[j] != 0 && protect < limit)return true;

			}

			else if (text_[i] == '9') {

				for (; text_[i] == '9' && i < val_len; i++, limit++);

				for (; isdigit(old_str[j]) && j < str_len; j++);

			}

			else if (text_[i] == '?') {

				for (; text_[i] == '?' && i < val_len; i++, limit++);

				for (; isalpha(old_str[j]) && j < str_len; j++);

			}

		}

		return false;

	}*/
